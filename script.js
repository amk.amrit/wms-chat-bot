// Create chat icon element
var chatIcon = document.createElement("div");
chatIcon.classList.add("chat-icon");
document.body.appendChild(chatIcon);

// Style chat icon
chatIcon.style.position = "fixed";
chatIcon.style.bottom = "20px";
chatIcon.style.right = "20px";
chatIcon.style.width = "50px";
chatIcon.style.height = "50px";
chatIcon.style.backgroundColor = "#007bff";
chatIcon.style.borderRadius = "50%";
chatIcon.style.display = "flex";
chatIcon.style.justifyContent = "center";
chatIcon.style.alignItems = "center";
chatIcon.style.cursor = "pointer";

// Create chat icon image
var chatIconImg = document.createElement("img");
chatIconImg.src = "chat-icon.png";
chatIconImg.alt = "Chat Icon";
chatIconImg.style.width = "80%";
chatIconImg.style.height = "auto";
chatIcon.appendChild(chatIconImg);
